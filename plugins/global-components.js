import Vue from 'vue'

import LoadingIcon from '~/components/common/loading-icon'
Vue.component('LoadingIcon', LoadingIcon)

import CopangItemInfo from '~/components/copang-item-info'
Vue.component('CopangItemInfo', CopangItemInfo)
